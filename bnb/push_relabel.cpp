#include <limits>
#include <gp-bnb/push_relabel.hpp>


// initialization of sources and sinks not elegant, but useful
push_relabel::push_relabel(const graph &g) 
    : g_(g) {
};

void push_relabel::reset(std::vector<node_id>& sources, std::vector<node_id>& sinks) {
	
	sources_ = &sources;
	sinks_ = &sinks;

	// Initial flow value
	flow_value_ = 0;

    // Initialize all vectors
    labels_.assign(g_.num_nodes() + 1, 0);
    flow_.assign(g_.num_nodes() + 1, std::vector<int>(g_.num_nodes() + 1, 0));
    excess_.assign(g_.num_nodes() + 1, 0);
    sources_and_sinks_.assign(g_.num_nodes() + 1, 0);

    // Sources marked as 1, sinks marked as -1. All the other nodes are 0.
	for (unsigned int i = 0; i < sources_->size(); ++i) {
		sources_and_sinks_[sources_->operator[](i)] = 1;
	}
	for (unsigned int i = 0; i < sinks_->size(); ++i) {
		sources_and_sinks_[sinks_->operator[](i)] = -1;
	}

};

void push_relabel::push(node_id u, node_id v) {

    int d = std::min(excess_[u], 1 - flow_[u][v]);
    flow_[u][v] += d;
    flow_[v][u] -= d;
    excess_[u] -= d;
    excess_[v] += d;

    if (d && excess_[v] == d) {
        excess_nodes_.push(v);
    }
       
}


void push_relabel::relabel(node_id u) {

    int d = std::numeric_limits<int>::max();

    const std::vector<node_id> & neighbors = g_.get_adjacency(u);

    for (auto v : neighbors) {
        if (1 - flow_[u][v] > 0)
            d = std::min(d, labels_[v]);
    }

    if (d < std::numeric_limits<int>::max())
        labels_[u] = d + 1;

}


void push_relabel::discharge(node_id u) {

    while (excess_[u] > 0) {

        const std::vector<node_id> & neighbors = g_.get_adjacency(u);

        bool push_possible = false;
        for (auto v : neighbors) {
            if (1 - flow_[u][v] > 0 && labels_[u] > labels_[v]) {
                push(u, v);
                push_possible = true;
            }
        }

        if (!push_possible)
             relabel(u);
    }

}

/* Preflow: Labels for source nodes - |V|.
Excess for source nodes - inf.
Push all outgoing edges from source nodes. */
void push_relabel::preflow() {

    int num_nodes = g_.num_nodes();

	for (unsigned int i = 0; i < sources_->size(); ++i) {
		node_id s = sources_->operator[](i);
        labels_[s] = num_nodes;
        excess_[s] = std::numeric_limits<int>::max();
    }

	for (unsigned int i = 0; i < sources_->size(); ++i) {
		node_id s = sources_->operator[](i);
        const std::vector<node_id> & neighbors = g_.get_adjacency(s);
        for (auto n : neighbors) {
            if (sources_and_sinks_[n] != 1) // if n is not a source node
                push(s, n);
        }
    }
}

void push_relabel::run() {

    preflow();

    while (!excess_nodes_.empty()) {
        int u = excess_nodes_.front();
        excess_nodes_.pop();

        // if u is not a source and not a sink
        if (sources_and_sinks_[u] == 0)
            discharge(u);
    }

    // Flow value is the sum of excesses in sinks
	for (unsigned int i = 0; i < sinks_->size(); ++i) {
        flow_value_ += excess_[sinks_->operator[](i)];
	} 
}

