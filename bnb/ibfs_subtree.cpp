#include <gp-bnb/ibfs_subtree.hpp>

#include <cassert>

ibfs_subtree::ibfs_subtree(int id, const graph& g, std::vector<bool>& flow_edges) : id_(id), 
	g_(g), unassigned_(g.num_nodes()), labels_(std::vector<label>(g.num_nodes())), pred_(std::vector<node_id>(g.num_nodes())), flow_edges_(flow_edges) {
}

void ibfs_subtree::reset(std::vector<node_id>& roots) {
	labels_.assign(g_.num_nodes(), unassigned_);
	pred_.assign(g_.num_nodes(), 0);
    for (const auto& n : roots) {
        labels_[n-1] = 0;
		current_front_.push_back(n);
    }

    current_max_ = 0; 
    max_adoption_label_ = 0;
    last_resid_orphans_.clear();
}

void ibfs_subtree::add_node(label l, node_id node, node_id pred) {
    assert(l == labels_[pred-1]+1);
    labels_[node-1] = l;
    pred_[node-1] = pred;
	if (l == max_adoption_label_) current_front_.push_back(node);
}

std::vector<node_id> ibfs_subtree::get_front() {
    std::vector<node_id> front = current_front_;
	current_front_.clear();
    return front;
}

std::vector<node_id> ibfs_subtree::reduce_path(node_id leaf) {
    // Assertion: augmentation paths are exclusively found on the leaves
    assert(labels_[leaf-1] == current_max_);

    last_resid_orphans_.clear();

    std::vector<node_id> flow_nodes;
    node_id current = leaf;

    // build a vector with all nodes of the flow from the leaf to the immediate child of the root
    for (label l = labels_[current-1]; l > 0; --l) {
        node_id pred = pred_[current-1];
        pred_[current-1] = 0;
        assert(labels_[current-1] == labels_[pred-1]+1);
        flow_nodes.push_back(current);
		flow_edges_[g_.get_edge_id(current, pred)-1] = true;
        current = pred;
    }

	std::vector<node_id> new_front;
    for (label l = flow_nodes.size(); l > 0; l--) {
        node_id orphan = flow_nodes[l-1];
        label old_label = labels_[orphan-1];
        label new_label = adopt(orphan);

        // change pred but do not adopt childs of this orphan
        if (new_label == old_label) {
            if (new_label == current_max_) new_front.push_back(orphan);
        }

        // processes all successors as orphans if this orphan can't be adopted on origin label
        else {
            if (new_label == 0) last_resid_orphans_.push_back(orphan);
            else if (new_label == current_max_) new_front.push_back(orphan);
            
            std::vector<node_id> parents { orphan };
            std::vector<node_id> childs;
            while (!parents.empty()) {
                // searches all childs of the current parents
                for (auto& parent : parents) {
                    for (node_id potential_child : g_.get_adjacency(parent)) {
                        if (pred_[potential_child-1] == parent) childs.push_back(potential_child);
                    }
                }
                // tries to adopt all found childs
                for (auto& child : childs) {
                    bool is_front = false;
                    if (labels_[child-1] == current_max_) is_front = true;
                    label new_child_label = adopt(child);
                    if (new_child_label == 0) last_resid_orphans_.push_back(child);
                    else if (new_child_label == current_max_ && !is_front) new_front.push_back(orphan);
                }
                parents = childs; 
                childs.clear();
            }
        }
         
    } 
    return new_front;
}

// tries to insert orphans with minimum label
// if they could have been inserted with label <= min,
// they would have happened because of BFS
label ibfs_subtree::adopt(node_id orphan) {
	label min = labels_[orphan-1];
    label current_min = unassigned_;
    node_id new_pred = 0;

	const auto& adjacency = g_.get_adjacency(orphan);
	const auto& edge_ids = g_.get_edge_ids(orphan);
    for (node_id i = 0; i < adjacency.size(); ++i) {
		node_id n = adjacency[i];
        if (labels_[n-1] == min-1 && !flow_edges_[edge_ids[i]-1]) {
            add_node(min, orphan, n);
            return min;
        } else if (labels_[n-1]+1 < current_min 
                && labels_[n-1] < max_adoption_label_
                && !flow_edges_[edge_ids[i]-1]) {
            current_min = labels_[n-1]+1;
            new_pred = n;
        }
    }
    if (new_pred != 0) {
        add_node(current_min, orphan, new_pred);
        return current_min;
    } else {
        labels_[orphan-1] = unassigned_;
        pred_[orphan-1] = 0;
        return 0;
    }
}

