#include <cassert>

#include <gp-bnb/partition.hpp>

partition::partition(graph& g) : graph_(g) {
    // Assigns all nodes to none
    node_assignments_ = std::vector<partition::subgraph>(g.num_nodes(), none);

    // Initializes node counting map
    nodes_ = std::vector<unsigned int> { 0, 0, g.num_nodes() };
}

void partition::assign_node(node_id v, subgraph sg) {
    assert(node_assignments_[v-1] == none);
    
    // Increments current objectives
    for (auto const& target : graph_.get_adjacency(v)) {
        if (node_assignments_[target-1] == (sg+1)%2) {
            current_objective_++;    
        }
    }
    
    // Assigns node to subgraph
    node_assignments_[v-1] = sg;
    nodes_[sg]++;
    nodes_[none]--;
}

void partition::unassign_node(node_id v) {
    subgraph sg = node_assignments_[v-1];
    
    assert(sg != none);
    
    // Decrements current objectives
    for (auto const& target : graph_.get_adjacency(v)) {
        if (node_assignments_[target-1] == (sg+1)%2) {
            current_objective_--;
        }
    }
    
    // Reverts assignment to subgraph 
    node_assignments_[v-1] = none;
    nodes_[sg]--;
    nodes_[none]++;
}
