#include <algorithm>
#include <gp-bnb/greedy_packing.hpp>
#include <iostream>

bool do_swap = true;

greedy_packing::greedy_packing(graph& g, incremental_bfs& ibfs, bool with_flow)
    : graph_(g), i_bfs(ibfs), with_flow_(with_flow){        
        max_a = (graph_.num_nodes())/2;
};

void greedy_packing::reset(std::vector<node_id>& a, std::vector<node_id>& b){
    a_ = &a;
	b_ = &b;
	x_.clear();
	partitioning.clear();
    in_part.assign(graph_.num_nodes()+2, -1);
	a_count = a_->size();
    flow_ = bound = 0;

    visited.assign(graph_.num_nodes() +1, false);

	if(do_swap && a.size() > b.size()){
        swapped = true;
    	std::swap(a_, b_);
        //std::cerr << "swapped" << std::endl;
    }
    else{
        swapped = false;
    }
}

//breadth-first-search to determine neighbors of B
void greedy_packing::bfs(){

    for (node_id i = 0; i < b_->size(); ++i) {
		node_id node = b_->operator[](i);
        q.push(node);
        visited[node] = true;
    }

    for (node_id i = 0; i < a_->size(); ++i) {
        visited[a_->operator[](i)] = true;
    }

    while(!q.empty()){
        node_id node = q.front();
        q.pop();

        const auto& adjacency = graph_.get_adjacency(node);
		const auto& edge_ids = graph_.get_edge_ids(node);

        for(node_id i = 0; i < adjacency.size(); ++i){
			node_id v = adjacency[i];
            if(visited[v] == false){
                //falls nicht(mit_flow angegeben und knoten unter den flow_edges ist)dann push in zu x
                if(!with_flow_ || !flow_edges_->operator[](edge_ids[i]-1) ){
                    x_.push_back(v);
                    visited[v] = true;
                }
            }
            
        }
    }

    return;
};

void greedy_packing::run(){

    if(with_flow_){
        //get all flow edges and the flow
        i_bfs.reset(*a_, *b_);
        i_bfs.run();
        flow_ = i_bfs.get_max_flow();
		flow_edges_ = &i_bfs.get_flow_edges();
    }


    a_count = a_->size();
    if(a_count >= max_a)
        return;

    //x per bfs konstruieren
    bfs();

    //P konstruieren
    partitioning.resize(x_.size());
    
    //dafür für jeden nachbarn von B einen eigenen Block
    for(unsigned int i = 0; i <x_.size(); i++){
        partitioning[i].push_back(x_[i]);
        in_part[x_[i]] = i;
    }

    //dann jeweils kleinsten block um einen nachbarn erweitern
    //baue mit B zusammenhängende Partitionen (noch ohne lokale Suche) und sortiere nach Größe absteigend
    bool found_new_element = true;
    while (found_new_element) {
        found_new_element = false;
        for (unsigned int i = 0; i < partitioning.size(); i++){
            node_id v = partitioning[i].back();

			const auto& adjacency = graph_.get_adjacency(v);
			const auto& edge_ids = graph_.get_edge_ids(v);

			for(node_id u = 0; u < adjacency.size(); ++u){
				node_id node = adjacency[u];
                if (!visited[node]) {
                    if(!with_flow_ || !(flow_edges_->operator[](edge_ids[u]-1) )){
                        partitioning[i].push_back(node);
                        visited[node] = true;
                        in_part[node] = i;
                        found_new_element = true;
                        break;
                    }
                }
            }
        }
    }

    //danach lokale suche um balance zu verbessern ?

    std::sort(partitioning.begin(), partitioning.end(), [](std::vector<node_id>& x, std::vector<node_id>& y) {
        return x.size() > y.size();
    });
    
    // behandle von B aus unnerreichbare Knoten
    for (node_id i = 1; i <= graph_.num_nodes(); i++) {
        if (!visited[i]) {
            a_count++;
            if (a_count >= max_a){
                
                return;
            }
        }
    }

    // packe greedy (erst hier wird flow erhöht)
    for (auto& partition : partitioning) {
        bound++;
        a_count += partition.size();
        if (a_count >= max_a){
            return;
        }
    }

    return;
};

partition::subgraph greedy_packing::forced_assignment(node_id node, unsigned int best){
    unsigned int new_bound, x_v, p, index, neighbors;
    x_v = p = neighbors = 0;
    
    a_count = a_->size();

    //fall 1: node in nicht trivialem block
    if(visited[node]){

        //return partition::subgraph::none;
        
		const auto& adjacency = graph_.get_adjacency(node);
        //für jeden nachbarn von node
        for(unsigned int i = 0; i < adjacency.size(); ++i){
            node_id u = adjacency[i];
            //std::cerr << "node: " << u << std::endl;
            //knoten im selben block wie node ignorieren?!
            if(in_part[u] != -1 && in_part[u] != in_part[node]){
                //falls er in einem noch nicht betrachteten block liegt erhöhe x_v
                x_v++;
                index = in_part[u];
                //dieser block ist nun nicht trivial (jeden knoten nichtmehr
                //als visited markieren)
                for(node_id v : partitioning[index]){
                    //std::cerr << v << " ";
                    visited[v] = false;
                    in_part[v] = -1;
                }
                partitioning[index].clear();
            }
        }
        //x_v++;
        

        //neues eingeschränktes packing-bound berechnen
        std::sort(partitioning.begin(), partitioning.end(), [](std::vector<node_id> a, std::vector<node_id> b) {
        return a.size() > b.size();
        });
    
        // behandle von B aus unnerreichbare Knoten
        for (node_id i = 1; i <= graph_.num_nodes(); i++) {
            if (!visited[i]) {
                a_count++;
                if (a_count >= max_a){
                    break;
                }
            }
        }

        // packe greedy (erst hier wird flow erhöht)
        for (std::vector<node_id> partition : partitioning) {
            if (a_count >= max_a){
                break;
            }
            if(!partition.empty())
                p++;
            a_count += partition.size();
            
        }  
        new_bound = flow_ + x_v + p;
        if(new_bound >= best){
            if(swapped)
                return partition::sg_a;
            else
                return partition::sg_b;
        }
        return partition::subgraph::none;

    }
    else{
        //return partition::subgraph::none;
        //fall2: v in trivialem block 
        //bound erhöht sich um anzahl der nachbarn von v
        /*std::cerr << "a: ";
        for (node_id i = 0; i < a_->size(); ++i) {
		    node_id d = a_->operator[](i);
            std::cerr << d << " ";
        }
        std::cerr << "b: ";
        for (node_id i = 0; i < b_->size(); ++i) {
		    node_id d = b_->operator[](i);
            std::cerr << d << " ";
        }
*/
        const auto& edge_ids = graph_.get_edge_ids(node);
  //      std::cerr << "node " << node << " : ";
        const auto& adjacency = graph_.get_adjacency(node); 
        for(unsigned int i = 0; i < adjacency.size(); i++){
            node_id v = adjacency[i];
            //flow_edge test resolves loop
            if(!visited[v] && (!with_flow_ || !(flow_edges_->operator[](edge_ids[i]-1))) ){
               //std::cerr << " " << v;
                neighbors++;
            }
        }
       // std::cerr << std::endl;
        new_bound = flow_ + bound + neighbors ;
      //  std::cerr << "new bound: " << flow_ << " + " << bound << " + " << neighbors << " = " << new_bound << std::endl;
        
        if(new_bound >= best){
            if(swapped)
                return partition::sg_b;
            else
                return partition::sg_a;
        }
        return partition::subgraph::none;
    }
    return partition::subgraph::none;
}



