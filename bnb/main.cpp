#include <cassert>
#include <iostream>
#include <vector>

#include <gp-bnb/bnb.hpp>
#include <gp-bnb/graph.hpp>
#include <gp-bnb/partition.hpp>
#include <gp-bnb/metis_reader.hpp>

int main(int argc, char** argv) {
    assert(argc > 1);

    std::vector<std::string> args(argv, argv+argc);

    std::string graphFile = args[1];
    auto g = metis_reader().read(graphFile);

    std::cerr << "Number of nodes: " << g.num_nodes() << std::endl;

    std::string current_option;

	gp_bnb::lb lb_algo = gp_bnb::lb::none;

    for (int i = 2; i < argc; ++i) {
        if (i%2 == 0) {
            current_option = args[i];
        } else {
            if (current_option == "-l") {
                if (args[i] == "ek") { 
                    lb_algo = gp_bnb::lb::ek;
                } else if (args[i] == "ibfs") {
                    lb_algo = gp_bnb::lb::ibfs;
                } else if (args[i] == "pr") {
                    lb_algo = gp_bnb::lb::pr;
                } else if (args[i] == "gp") {
                    lb_algo = gp_bnb::lb::gp;
                } else if (args[i] == "fa") {
                    lb_algo = gp_bnb::lb::fa;
                }
            }
        }
    }

	auto sol = gp_bnb::solver(g, lb_algo);
    sol.solve();

    /*
    int i = 1;
    for(auto g : sol.best_solution()){
        
        std::cerr << i++ << ": " << g << std::endl;

    }
    */

}

