#include <limits>
#include <queue>
#include <cassert>
#include <iostream>

#include <gp-bnb/edmonds_karp.hpp>

edmonds_karp::edmonds_karp(const graph &g) 
    : g_(g) {
};

void edmonds_karp::reset(std::vector<node_id>& sources, std::vector<node_id>& sinks) {
	sources_ = &sources;
	sinks_ = &sinks;

	// Initial flow value
	flow_ = 0;

	// Initialize vectors
	sources_and_sinks_.assign(g_.num_nodes() + 1, 0);
	flow_edges_.assign(g_.num_edges(), false);
	
	// Sources marked as 1, sinks marked as -1. All other nodes are 0.
	for (unsigned int i = 0; i < sources_->size(); ++i) {
		sources_and_sinks_[sources_->operator[](i)] = 1;
	}
	for (unsigned int i = 0; i < sinks_->size(); ++i) {
		sources_and_sinks_[sinks_->operator[](i)] = -1;
	}

};

/* Breadth-first search in the graph from source nodes to sink nodes */
node_id edmonds_karp::bfs(std::vector<std::pair<node_id, edge_id>> &pred) const {

	pred.clear();
	pred.assign(g_.num_nodes() + 1, std::make_pair(0, 0)); // undiscovered nodes are marked with 0

	// Push all source nodes to the queue
	std::queue<node_id> q;

	for (unsigned int i = 0; i < sources_->size(); ++i) {
		node_id s = sources_->operator[](i);
		q.push(s);
		pred[s] = std::make_pair(s, 0);
	}

	// Perform BFS from each source node and stop when the sink was reached
	// or there was no path found from the source to the sink
	while (!q.empty()) {
		node_id u = q.front();
		q.pop();

		const auto& adjacency = g_.get_adjacency(u);
		const auto& edge_ids = g_.get_edge_ids(u);
		// iterate through neighbors of u
		for (node_id i = 0; i < adjacency.size(); ++i) {
			node_id v  = adjacency[i];
			edge_id eid = edge_ids[i];

			if(!flow_edges_[eid-1] && pred[v].first == 0) { 
				pred[v] = std::make_pair(u, eid);
	
				if (sources_and_sinks_[v] == -1) return v;

				q.push(v);
			}
		}
	}

	return 0;
};

// Edmonds-Karp Algorithm for unweighted, undirected graphs
void edmonds_karp::run() {

	while (true) {
		std::vector<std::pair<node_id, edge_id>> pred;

		// BFS from source nodes to sink nodes
		node_id n = bfs(pred);

		if (n == 0) break;

		flow_++;

		while (true) {
			flow_edges_[pred[n].second-1] = true;
			if (sources_and_sinks_[(n = pred[n].first)] == 1) break;
		}
	}
	
};

