#define CATCH_CONFIG_MAIN // It tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>

#include <gp-bnb/graph.hpp>

// Tests for Graph data structure

// Example 1.
TEST_CASE("GraphStructureTest") {

    std::vector<std::vector<unsigned int>> adjacencies {
		{2, 3}, 	//1
		{1, 3, 4},	//2
		{1, 2},		//3
		{2}			//4
    };
    
    graph g = graph(adjacencies);

    REQUIRE(g.num_nodes() == 4);
	REQUIRE(g.get_edge_id(1,2) == g.get_edge_id(2,1));
	REQUIRE(g.get_edge_id(1,3) == g.get_edge_id(3,1));
	REQUIRE(g.get_edge_id(2,4) == g.get_edge_id(4,2));

}
