#ifndef PARTITION_HPP
#define PARTITION_HPP

#include <vector>
#include <gp-bnb/graph.hpp>

using node_id = unsigned int;

// Represents potential solutions
class partition {
public:
    enum subgraph { sg_a = 0, sg_b = 1, none = 2 };

    /** @brief  Initializes an empty partition
     *  @param  Pointer to the graph to be partitioned
     */
    partition(graph& g);
    
    /** @brief  Gets the number of nodes of an element of the partition
     *  @param  Subgraph A, B or none
     *  @return Number of nodes of sg
     */
    unsigned int num_nodes_of(subgraph sg) const {
        return nodes_.at(sg);
    };

    /** @brief  Gets the number of edges between subgraph A and B
     *  @return Objective to be minimized
     */
    unsigned int current_objective() const {
        return current_objective_;
    };

    /** @brief  Gets the assignment of a node
     *  @param  Node Id
     *  @return Subgraph A, B or none
     */
    subgraph assigned_subgraph_of(node_id v) const {
        return node_assignments_[v-1];
    };

    /** @brief  Assigns a node to a subgraph
     *  @param  Node Id
     *  @param  Subgraph A or B
     */
    void assign_node(node_id v, subgraph sg); 
    
    /** @brief  Reverts the assignment of a node to a subgraph
     *  @param  Node Id
     */
    void unassign_node(node_id v);

private:
    graph graph_;
    std::vector<subgraph> node_assignments_;
    std::vector<unsigned int> nodes_;
    unsigned int current_objective_ = 0;
};

#endif
