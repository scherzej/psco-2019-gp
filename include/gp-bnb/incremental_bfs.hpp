#ifndef INCREMENTALBFS_H_
#define INCREMENTALBFS_H_

#include <vector>
#include "ibfs_subtree.hpp"
#include "graph.hpp"


class incremental_bfs {
public:
    /** @brief  Initializes an incremental BFS instance
     *  @param  Graph to be partitioned
     */
    incremental_bfs(const graph& g);

    /** @brief  Resets the members and the subtrees
     *  @param  List of new sources for S-T-cut
     *  @param  List of new sinks for S-T-cut
     */
    void reset(std::vector<node_id>& sources, std::vector<node_id>& sinks);
    
    /** @brief  Executes the IBFS algorithm
     */
    void run();

    /** @brief  Returns the flow value after execution, which corresponds to the minimal S-T-cut
     */
    unsigned int get_max_flow() const {
        return flow_;
    };

    /** @brief  Returns edges with flow > 0
     */
    std::vector<bool>& get_flow_edges() {
		return flow_edges_;
	}; 

private:
    enum subtree { s = -1, t = 1, none, s_root, t_root };
   
    const graph& g_;
    
    ibfs_subtree s_;
    ibfs_subtree t_;

    std::vector<subtree> node_assignments_;
    
    unsigned int flow_ = 0;
    std::vector<bool> flow_edges_;

    bool grow(ibfs_subtree& st);
};

#endif /* INCREMENTALBFS_H_ */
