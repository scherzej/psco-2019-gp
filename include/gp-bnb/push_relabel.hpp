#ifndef PUSHRELABEL_H_
#define PUSHRELABEL_H_

#include <vector>
#include <map>
#include <queue>
#include <gp-bnb/graph.hpp>

class push_relabel {
private:
	const graph &g_;

	std::vector<node_id>* sources_;
	std::vector<node_id>* sinks_;
	std::vector<int> sources_and_sinks_;

    std::vector<int> labels_, excess_;
    std::vector<std::vector<int>> flow_;
    std::queue<node_id> excess_nodes_;

    int flow_value_;

    void push(node_id u, node_id v);

	void preflow();

    void relabel(node_id u);

    void discharge(node_id u);

public:
	/**
	 * Constructs an instance of the Push-Relabel algorithm for the given graph, sources and sinks
	 * @param graph The graph.
	 * @param sources Vector of source nodes.
	 * @param sinks Vector of sink nodes.
	 */
	push_relabel(const graph &g);

	/** @brief  Resets the members and the subtrees
     *  @param  List of new sources for S-T-cut
     *  @param  List of new sinks for S-T-cut
     */
    void reset(std::vector<node_id>& sources, std::vector<node_id>& sinks);

	/**
	 * Computes the maximum flow, executes Push-Relabel algorithm with discharge.
	 * For unweighted, undirected Graphs.
	 */
	void run();

	/**
	 * Returns the value of the maximum flow from sources to sinks.
	 * @return The maximum flow value
	 */
	int get_max_flow() const {
		return flow_value_;
	};
};

#endif /* PUSHRELABEL_H_ */
