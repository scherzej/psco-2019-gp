#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <vector>

using node_id = unsigned int; 
using edge_id = unsigned int;

// Represents the input graph to be partitioned
class graph {
public:    

    /** @brief  Initializes the graph to be partitioned
     *  @param  Adjacency lists
     */
    graph(std::vector<std::vector<unsigned int>> a);

    /** @brief  Returns the number of nodes of the graph
     *  @return Number of nodes
     */
    unsigned int num_nodes() const {
        return nodes_;
    };

    /** @brief  Returns the number of edges in the graph
     *  @return Number of edges after have been indexed
     */
    unsigned int num_edges() const {
        return edges_;
    };

    /** @brief  Provides access to the adjacency of a node  
     *  @param  Node Id
     *  @return Adjacency of the node
     */
    const std::vector<unsigned int>& get_adjacency(node_id v) const {
        return adjacency_list_[v-1];
    };

    /** @brief  Provides access to the edge ids of a node  
     *  @param  Node Id
     *  @return Edge ids of the node
     */
    const std::vector<unsigned int>& get_edge_ids(node_id v) const {
        return indexed_edges_[v-1];
    };

    /** @brief  Returns the id of an edge after calling index_edges()
     *  @param  Two Node ids (u, v)
     *  @return Edge id
     */
    edge_id get_edge_id(node_id u, node_id v) const;

private:
    unsigned int nodes_;
	unsigned int edges_;
	std::vector<std::vector<unsigned int>> adjacency_list_;
    std::vector<std::vector<edge_id>> indexed_edges_;
};

#endif
