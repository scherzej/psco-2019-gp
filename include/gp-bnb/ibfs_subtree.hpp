#ifndef IBFSSUBTREE_HPP_
#define IBFSSUBTREE_HPP_

#include <unordered_set>
#include <vector>
#include "graph.hpp"

using label = unsigned int; 
using node_id = unsigned int;

class ibfs_subtree {
public:    

    /** @brief  Initializes a subtree for incremental bfs
     *  @param  List of roots (sources or sinks)
     */
    ibfs_subtree(int id, const graph& g, std::vector<bool>& flow_edges);

    /** @brief  Resets the subtree with new roots
     *  @param  List of new roots
     */
    void reset(std::vector<node_id>& roots);
    
    /** @brief  Deletes the path of the new flow of this subtree 
     *          Part of the augmentation of this subtree
     *  @param  Leaf with edge to the leaf of the other tree along the augmenting path 
     *  @return List of inserted orphans with label current_max_
     */
    std::vector<node_id> reduce_path(node_id leaf); 

    /** @brief  Adds a node to the subtree with label l as a child of pred 
     *  @param  Label of the node to be added
     *  @param  Node to be added
     *  @param  Predecessor of the node to be added
     */
    void add_node(label l, node_id node, node_id pred);

    std::vector<node_id> get_front(); 

    label get_current_max() {
        return current_max_;
    };

    void set_max_adoption_label(label l) {
        max_adoption_label_ = l;
    };
    
    int get_id() const {
        return id_;
    };

    void increment_current_max() {
        current_max_++;
    };
    
    const std::vector<node_id>& get_last_resid_orphans() const {
        return last_resid_orphans_;
    };

    bool is_front_element(node_id node) const {
        return labels_[node-1] == current_max_;
    };

private:
    int id_;
    const graph& g_;
    const label unassigned_;

    std::vector<label> labels_;
    std::vector<node_id> pred_;

	std::vector<node_id> current_front_;

    label current_max_ = 0;
    label max_adoption_label_ = 0;
    
    std::vector<bool>& flow_edges_;

    std::vector<node_id> last_resid_orphans_;
    
    label adopt(node_id orphan);
};

#endif /* IBFSSUBTREE_HPP_ */
