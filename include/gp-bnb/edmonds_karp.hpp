#ifndef EDMONDSKARP_H_
#define EDMONDSKARP_H_

#include <vector>
#include <gp-bnb/graph.hpp>

class edmonds_karp {
private:
	const graph &g_;

	std::vector<node_id>* sources_;
	std::vector<node_id>* sinks_;
	std::vector<int> sources_and_sinks_;

	unsigned int flow_;
	std::vector<bool> flow_edges_;


	/**
	 * Performs a breadth-first search on the graph from the source node to find an augmenting path to the sink node respecting the flow values
	 * @param pred Used to store the path from the source to the sink.
	 * @return The sink node.
	 */
	 node_id bfs(std::vector<std::pair<node_id ,edge_id>> &pred) const;

public:
	/**
	 * Constructs an instance of the EdmondsKarp algorithm for the given graph
	 * @param graph The graph.
	 */
	 edmonds_karp(const graph &g);

	 /** @brief  Resets the members and the subtrees
     *  @param  List of new sources for S-T-cut
     *  @param  List of new sinks for S-T-cut
     */
    void reset(std::vector<node_id>& sources, std::vector<node_id>& sinks);

	/**
	 * Computes the maximum flow, executes the EdmondsKarp algorithm.
	 * For unweighted, undirected Graphs.
	 */
	void run();

	/**
	 * Returns the value of the maximum flow from source to sink.
	 *
	 * @return The maximum flow value
	 */
	int get_max_flow() const {
		return flow_;
	};
};

#endif /* EDMONDSKARP_H_ */
