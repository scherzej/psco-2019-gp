#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <gp-bnb/graph.hpp>

#ifndef METISREADER_H_
#define METISREADER_H_

/** Reader for the METIS file format **/

class metis_reader {
    public:
        metis_reader() = default;

        graph read(std::string& path);
};

#endif 