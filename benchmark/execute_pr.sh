#!/bin/bash

BIN=../build/gp-bnb
OPT="-l pr"
TIMEOUT=1800

if [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
    echo "Aufruf: sh execute.sh [PATH-TO-BINARY]"
    exit 0
elif [ -n "$1" ]; then 
    BIN=$1 
fi

for i in $(ls sample); do
    FILE=sample/${i}
    NAME="$(echo "${i}" | sed 's/^.*_//' | sed 's/.graph//')"
    IFS=' ' read -r -a A <<< "$(head -n 1 ${FILE})"
    echo "##################################################################################"
    echo "Start partitioning of graph ${NAME} (#nodes:${A[0]};#edges:${A[1]})"
    echo "##################################################################################"
    timeout ${TIMEOUT} ${BIN} ${FILE} ${OPT} 
    echo ""
done
exit 0

