zum Kompilieren wird meson und ein C++-Kompiler benötigt

- Repository klonen
- in das Projektverzeichnis wechseln
- mit $ meson ${builddir} das Build-Verzeichnis erzeugen (mit Option 
    --buildtype=release wird compilerseitige Optimierung durchgeführt)
- ins Build-Verzeichnis wechseln
- kompilieren mit $ ninja
- Befehl $ ninja test zum ausführen der Testsuite

Um Partitionierung auf einem gegebenem Graphen aus dem Projektverzeichnis heraus zu berechnen:
- $ ${builddir}/gp-bnb ${pfad-zum-graphen}
- fürs Auswählen eines Flow-Algorithmus wird zusätzlich die Option -l mit ek (für Edmonds-Karp), pr (für Push-Relabel), ibfs (für Incremental-BFS), gp (für Greedy-Packing) oder fa(für Forced Assignements zusätzlich zu Greedy-Packing) benötigt